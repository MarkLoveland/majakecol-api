#Monteization

*Users will pay a monthly subscription to have access to an API key.
These keys would be generated using UUID4 and listed in a sheltered endpoint within the database.
Users would need a key to submit jobs to the API, but would be able to query datapoints.
Keys would cycle monthly, with subscribers being provided a new one upon payment clearing.

To use a key, users would would enter their value into our front end client, which would include it in the requests sent to the flask container.
The server, in turn, compares the key to those listed in the database. 
If the key is recognized, the API will allow job posting. Else, it will return a 401 unauthorized user error.

*
