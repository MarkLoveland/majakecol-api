#MaJakeCol API Spec

##Jobs
###GET /jobs
returns all stored job objects
```
[
  {
    "create_time": "2018-12-07 01:48:49", 
    "end": "1869", 
    "last_update_time": "2018-12-07 01:48:49", 
    "start": 1775, 
    "status": "complete", 
    "uid": "743ebc4d-07d0-472f-96dd-1f224d75cdfe"
  }, 
  {
    "create_time": "2018-12-07 01:46:09", 
    "end": "1869", 
    "last_update_time": "2018-12-07 01:46:09", 
    "start": 1775, 
    "status": "In Progress", 
    "uid": "6f1177ed-4e48-4a2f-8280-278fb9951de5"
  }
]
```
Examples:
bash:
```
curl localhost:5000/jobs
```
python:
```
import requests
r = requests.get('http://localhost:5000/jobs')
```

###POST /jobs
Add a job object to the database
Either provide a start/end or offset/limit
```
{
   "start": 1775

}
```
Examples:
bash:
```
curl -d '{"start":1775}' localhost:5000/jobs
```
python:
```
import requests
payload = {"sunspots": "17", "year": "1945"}
r = requests.post('http://localhost:5000/sunspots', data=payload)
```
###GET /jobs/<jid>
Displays information tied to given jid
```
{
  "create_time": "2018-12-07 02:27:10", 
  "end": "1869", 
  "last_update_time": "2018-12-07 02:27:10", 
  "start": 1800, 
  "status": "complete", 
  "uid": "58f421b1-880d-494b-9463-0fd5e47b581d"
}
```
Examples:
bash:
```
curl localhost:5000/jobs/58f421b1-880d-494b-9463-0fd5e47b581d
```
python:
```
import requests
r=requests.get('localhost:5000/jobs/58f421b1-880d-494b-9463-0fd5e47b581d')
```
###GET /jobs/<jid>/plot
Returns png file with plot associated with given job id

Examples:
bash:
```
curl localhost:5000/jobs/58f421b1-880d-494b-9463-0fd5e47b581d/plot --output filename.png
```

###GET /jobs/<jid>/histogram
Returns png file with histogram associated with given job id

Examples:
bash:
```
curl localhost:5000/jobs/58f421b1-880d-494b-9463-0fd5e47b581d/plot --output filename.png
```


##Sunspots

###GET /sunspots  
returns whole data set
```
[
  {
    "id": 0, 
    "sunspots": "101", 
    "year": "1770"
  }, 
  {
    "id": 1, 
    "sunspots": "82", 
    "year": "1771"
  }, 
  ...
  ]
```

Examples:
bash
```
curl localhost:5000/sunspots

```
python
```
import requests
r = requests.get('http://localhost:5000/sunspots')
```

###POST /sunspots add a data point to the timeseries
```
{
  "sunspots" : "17",
  "year" : "2010"
}
```
Examples:
bash
```
curl -d '{"sunspots":17,"year":2010}' http://localhost:5000/sunspots
```
python
```
import requests
payload = {"sunspots": "17", "year": "1945"}
r = requests.post('http://localhost:5000/sunspots', data=payload)
```



###GET /sunpots?<start=<start>&end=<end>>
Gives all data as Json object from start year to end year
start and end are optional, one parameter is sufficient
```
[
  {
    "id": 96, 
    "sunspots": "16", 
    "year": "1866"
  }, 
  {
    "id": 97, 
    "sunspots": "7", 
    "year": "1867"
  }, 
  {
    "id": 98, 
    "sunspots": "38", 
    "year": "1868"
  }, 
  {
    "id": 99, 
    "sunspots": "74", 
    "year": "1869"
  }
]
```
Example:
bash
```
curl 'localhost:5000/sunspots?start=1866&end=1869'
```
python
```
import requests
r = requests.get('http://localhost:5000/sunspots?start=1866&end=1869')
```

###GET /sunspots?<limit=<limit>&<offset=<offset>>
returns all data starting at id of the offset
provides as many datapoints as limit given
```
[
  {
    "id": 5, 
    "sunspots": "7", 
    "year": "1775"
  }, 
  {
    "id": 6, 
    "sunspots": "20", 
    "year": "1776"
  }, 
  {
    "id": 7, 
    "sunspots": "93", 
    "year": "1777"
  }
]
```
bash
```
curl 'localhost:5000/sunspots?limit=3&offset=5'
```
python
```
import requests
r = requests.get('http://localhost:5000/sunspots?limit=3&offset=5')
```


###GET /sunspots/year/<year>
returns data only for given year
```
[
  {
    "id": 7, 
    "sunspots": "93", 
    "year": "1777"
  }
]


```
bash:
```
curl localhost:5000/sunspots/year/1777
```
python
```
import requests
r = requests.get('http://localhost:5000/sunspots/year/1777')
```
###GET /sunspots/year/minmax
returns minimum and maximum year
```
{
  "max": "1869", 
  "min": "1770"
}
```
Examples:
bash:
```
curl localhost:5000/sunspots/year/minmax
```

###GET /sunspots/id/<id>
returns data only for given id
```
[
  {
    "id": 55, 
    "sunspots": "17", 
    "year": "1825"
  }
]
```
Examples:
bash:
```
curl localhost:5000/sunspots/id/55
```
python
```
import requests
r = requests.get('http://localhost:5000/sunspots/id/55')
```
###GET /sunspots/minmax
returns data for minimum and maximum sunspots in entire database

```
[
  {
    "max": {
      "id": 8, 
      "sunspots": "154", 
      "year": "1778"
    }, 
    "min": {
      "id": 40, 
      "sunspots": "0", 
      "year": "1810"
    }
  }
]
```
Examples:
bash:
```
curl localhost:5000/jobs/minmax
```
python:
```
import requests
r = requests.get('http://localhost:5000/jobs/minmax')
```

