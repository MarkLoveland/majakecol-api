from flask import Flask, jsonify, request, send_file 
from read_sunspots import * 
import json
import io
import redis
from jobs import *
import os


REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0)
graphs = redis.StrictRedis(host=REDIS_IP,port=REDIS_PORT,db=2)

app=Flask(__name__)

@app.route('/jobs',methods=['POST','GET'])
def submit_jobs():
    print(request.method)
    if request.method=='POST':
        message1=request.get_json(force='TRUE')
        try:
            start1=message1['start']
            start1=int(start1)
            
            minmax=find_minmax()
            if start1<int(minmax['min']) or start1>int(minmax['max']):
                return "Start date out of range \n"
        except:
            start1=0
        try:
            end1=message1['end']
            end1=int(end1)
            minmax=find_minmax()
            if end1>int(minmax['max']) or end1<int(minmax['min']):
                return "End date out of range \n"
        except:
            end1=0
        try:            
            limit1=message1['limit']
            limit1=int(limit1)
            minmax=find_minmax()
            if limit1>minmax['numdatapoints'] or limit1<1:
                return "Limit is too high or incorrect \n"
        except:
            limit1=0
        try:
            offset1=message1['offset']
            offset1=int(offset1)
            mimax=find_minmax()
            if (offset1+limit1)>mimax['numdatapoints'] or offset1<0:
                return "Limit and/or offset is too high \n"
        except:
            offset1=0
        if (start1 or end1) and (limit1 or offset1):
            return "Error: You can't provide a start/end and limit/offset\n"
        elif start1 and end1:
            try:
                    start=int(start1)
                    end=int(end1)
                    jid=gen_jid()
                    add_job(jid,start,end)
                    return "Job Submitted with start and end parameters \n "
            except:
                    return "Error, must input an integer \n"
        elif start1:
            try:
                    start=int(start1)
                    jid=gen_jid()
                    add_job(jid,start)
                    return "Job Submitted with only start parameter \n "
            except:
                return "Error, must input an integer \n"
        elif end1:
            try:
                    start=int(start1)
                    end=int(end1)
                    jid=gen_jid()
                    add_job(jid,start,end)
                    return "Job Submitted with only an end parameter\n "
            except:
                return "Error, must input an integer \n"
        elif limit1 and offset1:
            try:
                jid=gen_jid()
                
                add_job(jid,offset=int(offset1),limit=int(limit1))
                return "Job Submitted with limit and offset \n"
            except:
                return "Error, invalid offset and/or limit parameter \n"
        elif limit1:
            try:
                jid=gen_jid()
                add_job(jid,limit=int(limit1))
                return "Job Submitted with limit only \n"
              
            except:
                return "Error, incorrect limit passed \n"
        elif offset1:
            try:
                jid=gen_jid()
                add_job(jid,offset=int(offset1))
                return "Job Submitted with offset \n"              
            except:
                return "Error, offset parameter incorrect \n"
        else:
            return "Error: either request a start/end or limit/offset \n"
    if request.method=='GET':
        return jsonify(get_alljobs())

@app.route('/jobs/<jid>',methods=['GET'])
def get_jobinfo(jid):
        try:
            return jsonify(get_job_jid(jid))
        except:
            return "Invalid Job ID"

@app.route('/jobs/<jid>/plot',methods=['GET'])
def give_plot(jid):
    plot = get_plot(jid)
    return send_file(io.BytesIO(plot[1][0]),
            mimetype='image/png',
            as_attachment=True,
            attachment_filename='{}.png'.format(jid))
    #return jsonify(get_plot(jid))
@app.route('/jobs/<jid>/histogram',methods=['GET'])
def give_hist(jid):
    hist=get_hist(jid)
    return send_file(io.BytesIO(hist[1][0]),
            mimetype='image/png',
            as_attachment=True,
            attachment_filename='{}.hist.png'.format(jid))

@app.route('/sunspots', methods=['POST','GET'])
def get_sunspots():
    if request.method=='GET':
        start1=request.args.get('start')
        end1=request.args.get('end')
        limit1=request.args.get('limit')
        offset1=request.args.get('offset')
        if (start1 or end1) and (limit1 or offset1):
            return "Error: You can't provide a start/end and limit/offset\n"
        elif start1 and end1:
            try:
                return jsonify(search_by_year(start=int(start1),end=int(end1)))
            except:
                    return "Error, must input an integer \n"
        elif start1:
            try:
                return jsonify(search_by_year(start=int(start1)))
            except:
                return "Error, must input an integer \n"
        elif end1:
            try:
                return jsonify(search_by_year(end=int(end1)))
            except:
                return "Error, must input an integer \n"
        elif limit1 and offset1:
            try:
                return jsonify(search_by_limit(limit=int(limit1),index=int(offset1)))
            except:
                return "Ayy Error, must input an integer \n"
        elif limit1:
            try:
                return jsonify(search_by_limit(limit=int(limit1)))
            except:
                return "Error, must input an integer \n"
        elif offset1:
            try:
                return jsonify(search_by_limit(index=int(offset1)))
            except:
                return "Error, must input an integer \n"
        else:
            return jsonify(return_whole_file())

    if request.method=='POST':
        message1=request.get_json(force='TRUE')
        try:
            year1=int(message1['year'])
        except:
            return "Must enter an integer for year and sunspots \n"
        try:
            sunspots1=int(message1['sunspots'])
        except:
            return "Must enter number of sunspots as an integer \n"
        code1=add_datapoint(year1,sunspots1)
        if code1==0:
            return "Succesfully submitted \n"
        else:
            return "Data point already exists for this year! \n"

@app.route('/sunspots/id/<id1>',methods=['GET'])
def getid_stuff(id1):
    try:
        return jsonify(search_by_limit(index=int(id1),limit=1))
    except:
        return "Must enter an id from 0 to 100 \n"

@app.route('/sunspots/year/<value>',methods=['GET'])
def get_year(value):
    try:
        return jsonify(search_by_year(start=int(value),end=int(value)))
    except:
        return "Error, please enter a valid number for year \n"

@app.route('/sunspots/year/minmax', methods=['GET'])
def minmax_year():
    the_other_things_we_want = find_minmax()

    return jsonify(the_other_things_we_want)

@app.route('/sunspots/minmax', methods=['GET'])
def minmax_sunspots():
    the_things_we_want = find_extrema()

    return jsonify(the_things_we_want)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
