def find_minmax():
    minmax=[999999999,0]
    count=0
    with open("sunspots.csv",'r') as data1:
        for line in data1:
            count=count+1
            stripped=line.strip()
            fixed_line=stripped.replace(" ","")
            splits=fixed_line.split(',')
            if int(splits[0])>int(minmax[1]):
                minmax[1]=splits[0]
            if int(splits[0])<int(minmax[0]):
                minmax[0]=splits[0]

    out1={"min":minmax[0], "max":minmax[1],"numdatapoints":count}
    return out1
   

def find_extrema():
    data = return_whole_file()
    minv = 999999
    maxv = 0
    for dicks in data:
        if int(dicks["sunspots"]) < minv: 
            minv = int(dicks["sunspots"])
            mindick = dicks

        if int(dicks["sunspots"]) > maxv: 
            maxv = int(dicks["sunspots"]) 
            maxdick = dicks

    return [{"min":mindick, "max":maxdick}]




def add_datapoint(year,sunspots):
    with open("sunspots.csv",'r') as data1:
        flag=0
        newline=str(year)+" , "+str(sunspots)+"\n"
        for line in data1:
            stripped=line.strip()
            fixed_line=stripped.replace(" ","")
            splits=fixed_line.split(',')
            if str(splits[0])==str(year):
                flag=1
                break
    with open("sunspots.csv","a") as data1:
        if flag==0:
            data1.write(newline)
            return 0
        else:
            return 1



def return_whole_file():
	out1=[]
	counter=0
	with open("sunspots.csv",'r') as data1: #open file
		for line in data1: #read file by line
			stripped=line.strip() #strip line of characters on either end
			fixed_line=stripped.replace(" ","") #remove spaces between commas
			splits=fixed_line.split(',') #create array of words separated by commas
			dict1={"id":counter, 	#assign to dictionary
				"year":splits[0],
				"sunspots":splits[1]}
			out1.append(dict1) #append to list
			counter=counter+1
	return(out1)
		
def search_by_year(start=0,end=0):
    out1=[]
    counter=0
    minmax=find_minmax()
    min1=minmax["min"]
    max1=minmax["max"]
    if start==0: start=int(min1)
    if end==0: end=int(max1)
    start=int(start)
    end=int(end)
    if end>=start and end<=int(max1) and start>=int(min1):
        with open("sunspots.csv",'r') as data1: #open file
            for line in data1: #read file by line
                stripped=line.strip() #strip line of characters on either end
                fixed_line=stripped.replace(" ","") #remove spaces between commas
                splits=fixed_line.split(',') #create array of words separated by commas
                if int(splits[0])>=start and int(splits[0])<=end:
                    dict1={"id":counter,    #assign to dictionary
                            "year":splits[0],
                            "sunspots":splits[1]}
                    out1.append(dict1) #append to listi
                counter=counter+1
    else:
        out1="End must be > start. Years available are 1770-1869" 
    return(out1)
def search_by_limit(index=0,limit=100):
    out1=[]
    counter=0
    with open("sunspots.csv",'r') as data1: #open file
        for line in data1: #read file by line
            stripped=line.strip() #strip line of characters on either end
            fixed_line=stripped.replace(" ","") #remove spaces between commas
            splits=fixed_line.split(',') #create array of words separated by commas
            if counter>=index and (counter<limit+index and counter<100):
                dict1={"id":counter,    #assign to dictionary
                        "year":splits[0],
                        "sunspots":splits[1]}
                out1.append(dict1) #append to lis
            counter=counter+1
        if out1:
            return(out1)
        else:
            return("Invalid index value")

