import uuid
from hotqueue import HotQueue
import datetime
import redis
import json
from read_sunspots import search_by_year, find_minmax 
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os 


REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT  = os.environ.get('REDIS_PORT')


def gen_jid():
    return str(uuid.uuid4())
def gen_job_key(jid):
    return 'job.{}'.format(jid)

def gen_job(jid,status="Submitted",start1=None,end1=None):
    Job_dict={

            'uid':jid,
            'status':status,
            'start':start1,
            'end':end1,
            'create_time':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'last_update_time':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            }
    return Job_dict

def save_job(job_key):
    rd.set(job_key)


rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0)
hotboi=HotQueue("queue",host=REDIS_IP,port=REDIS_PORT,db=1)
graphs=redis.StrictRedis(host=REDIS_IP,port=REDIS_PORT,db=2)

def add_job(jid,start=None,end=None,limit=None,offset=None,status="Submitted"):
    minmax=find_minmax()
    min1=minmax["min"]
    max1=minmax["max"]
    if start==None:start=min1
    if end ==None:end=max1
    if offset != None:
        start=int(start)+int(offset)
        start=str(start)
    if limit!=None:
        end=int(start)+int(limit)-1
        end=str(end)
    job_dict=gen_job(jid,status,start1=start,end1=end)
    print("OYY")
    rd.set(gen_job_key(jid),json.dumps(job_dict))
    hotboi.put(gen_job_key(jid))
    return job_dict

#problem3
def get_job_jid(jid):
    job_key=gen_job_key(jid)
    return json.loads(rd.get(job_key))
def get_job(job_key):
    #job_key=gen_job_key(jid)
    return json.loads(rd.get(job_key))

#problem4
def get_alljobs():
    alljobs=[]
    for key in rd.keys():
        alljobs.append(json.loads(rd.get(key.decode('utf-8'))))    
    return alljobs

def get_plot(jid):
   job_key=gen_job_key(jid)
   #return json.loads(graphs.get(job_key))
   return False, graphs.hmget(job_key,'plot')

def get_hist(jid):
    job_key=gen_job_key(jid)
    return False, graphs.hmget(job_key,'histogram')


def finalize_job(jid, file_path, hist_file):
    job = get_job(jid)
    job['status'] = 'complete'
    #job['plot'] = open(file_path, 'rb').read()
    rd.set(jid, json.dumps(job))
    graphs.hmset( jid, {'plot': open(file_path, 'rb').read()} )
    graphs.hmset( jid, {'histogram' : open(hist_file, 'rb').read()})

def update_job(job_key, status):
    job = get_job(job_key)
    if (job):
        job['status'] = status



#plot stuff 
def execute_job(jid):
    # jid is actually jobkey here 
    job = get_job(jid)
    # may need to go through the cases here 
    points = search_by_year(job['start'],job['end'])
    years = [int(p['year']) for p in points]
    sunspots = [int(p['sunspots']) for p in points]
    plt.scatter(years, sunspots)
    plt.plot(years, sunspots)
    plt.xlabel('Year') # Add a label to the x-axis.
    plt.ylabel('Sunspots') # Add a label to the y-axis.
    plt.title('Sunspots Over Time') # Add a graph title.
    tmp_file = '/tmp/{}.png'.format(jid)
    plt.savefig(tmp_file, dpi=150)
    plt.clf()
    plt.cla()
    plt.hist(sunspots)
    plt.xlabel('Sunspots')
    plt.ylabel('Frequency')
    plt.title('Histogram of Sunspots Over Time')
    hist_file='/tmp/{}.hist.png'.format(jid)
    plt.savefig(hist_file, dpi=150)
    plt.clf()
    plt.cla()
    finalize_job(jid, tmp_file, hist_file)
    





