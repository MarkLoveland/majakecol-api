from flask import Flask,jsonify, request
import json
import redis
import datetime
from hotqueue import HotQueue
from jobs import *
import matplotlib.pyplot as plt
import os

REDIS_IP = os.environ.get('REDIS_IP')
REDIS_PORT = os.environ.get('REDIS_PORT')

rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=0)
hotboi=HotQueue("queue",host=REDIS_IP,port=REDIS_PORT,db=1)
graphs=redis.StrictRedis(host=REDIS_IP,port=REDIS_PORT,db=2)

@hotboi.worker
def exec_job(jid):

    
    job_key=jid
    job_item=json.loads(rd.get(job_key))
    job_item['status']='In Progress'
    job_item['last_update_time']=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    rd.set(job_key,json.dumps(job_item))
   
    execute_job(jid)#setting status to complete in the executioner
    
exec_job()


